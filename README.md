# Three Doors

## About

### Author

Ryan E. Anderson

---

### Description

This application can be used to create a hallway consisting of three doors. When a door in the hallway is opened, 
numbers are revealed. When a door in the hallway is closed, information is hidden. Each door in the hallway is numbered 
according to a sequence of integers that begins with one and continues until a provided number is reached. When the 
hallway is closed, all doors are reset, and a score is displayed.

---

### Version

0.1.0

---

### License

Apache-2.0

---

## Components

### Door

This component behaves as a door usually does: it can be opened and closed. When a door is in an open state, information 
is revealed. When a door is in a closed state, information is hidden. In the context of this application, a door reveals 
numbers when it is opened. Each door is numbered.

```text
<Door/>
```

#### Properties

##### buttonPrimaryClassName

This property can be used to style the button that opens or closes a door.

##### doorOpenClassName

This class can be used to style a door that is in an open state.

##### doorClosedClassName

This class can be used to style a door that is in a closed state.

##### doorNumber

This property represents a door number. This is an integer that will be at least one.

##### doorText

This is the default text that is displayed on a door that is in a closed state.

#### State

##### doorClass

This class is used to style a button with either doorOpenClassName or doorClosedClassName.

##### doorOpen

This value determines whether or not a door is open.

##### score

This value is used to keep track of a numerical score that gets summed with an integer whenever a door is opened.

##### text

This value is used to present information on a door.

### Hallway

This component contains instances of Door. When a hallway is closed, the state of each door is reset.

```text
<Hallway/>
```

#### Properties

##### buttonSecondaryClassName

This property is used to style a button that closes a hallway.

##### buttonPrimaryClassName

This is used to set the property of the Door component that has the same name.

##### doorClosedClassName

This is used to set the property of the Door component that has the same name.

##### doorOpenClassName

This is used to set the property of the Door component that has the same name.

##### numberOfDoors

This property determines how many instances of the Door component will be created during the rendering process.

#### State

##### score

This is used to maintain a score that changes whenever a hallway is closed.

### App

This is the root component of the application. An instance of Hallway is nested within this component.

```text
<App/>
```

## Tests

This package contains various tests for evaluating the functionality of each component.