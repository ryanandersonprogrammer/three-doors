/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 Door Tests
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import {
    assertMatch,
    React,
    renderer,
    shallow,
    spyOnFunctionAfterButtonClick,
    spyOnSetState
} from "../../three-doors-test-setup";
import Door from "../../../src/components/Door";

describe("Door", () => {
    const doorNumber = 1;
    const buttonPrimaryClassName = "btn-test btn-primary-test";
    const buttonPrimaryClassNameCss = ".btn-test.btn-primary-test";
    const doorClosedClassName = "door-closed-test";
    const doorOpenClassName = "door-open-test";
    const doorText = "This is text for a test.";
    const door = <Door doorNumber={doorNumber} buttonPrimaryClassName={buttonPrimaryClassName}
                       doorClosedClassName={doorClosedClassName} doorOpenClassName={doorOpenClassName}
                       doorText={doorText}/>;

    it("Renders correctly when doorNumber is supplied", () => {
        const component = renderer.create(
            <Door doorNumber={doorNumber}/>
        );

        assertMatch(component);
    });

    it("Renders correctly when doorNumber and buttonPrimaryClassName are supplied", () => {
        const component = renderer.create(
            <Door doorNumber={doorNumber} buttonPrimaryClassName={buttonPrimaryClassName}/>
        );

        assertMatch(component);
    });

    it("Renders correctly when doorNumber, buttonPrimaryClassName, and doorClosedClassName are supplied",
        () => {
            const component = renderer.create(
                <Door doorNumber={doorNumber} buttonPrimaryClassName={buttonPrimaryClassName}
                      doorClosedClassName={doorClosedClassName}/>
            );

            assertMatch(component);
        });

    it("Renders correctly when doorNumber, buttonPrimaryClassName, doorClosedClassName, and doorOpenClassName " +
        "are supplied", () => {
        const component = renderer.create(
            <Door doorNumber={doorNumber} buttonPrimaryClassName={buttonPrimaryClassName}
                  doorClosedClassName={doorClosedClassName} doorOpenClassName={doorOpenClassName}/>
        );

        assertMatch(component);
    });

    it("Renders correctly when doorNumber, buttonPrimaryClassName, doorClosedClassName, doorOpenClassName, " +
        "and doorText are supplied", () => {
        const component = renderer.create(
            <Door doorNumber={doorNumber} buttonPrimaryClassName={buttonPrimaryClassName}
                  doorClosedClassName={doorClosedClassName} doorOpenClassName={doorOpenClassName} doorText={doorText}/>
        );

        assertMatch(component);
    });

    it("Invokes open() once when a certain button is clicked and the state of doorOpen is false", () => {
        spyOnFunctionAfterButtonClick(door, "open", 1, buttonPrimaryClassNameCss);
    });

    it("Invokes close() once when a certain button is clicked and the state of doorOpen is true", () => {
        spyOnFunctionAfterButtonClick(door, "close", 1, buttonPrimaryClassNameCss,
            (wrapperInstance) => {
                wrapperInstance.state.doorOpen = true;
            });
    });

    it("Does not invoke open() when a certain button is clicked and the state of doorOpen is true", () => {
        spyOnFunctionAfterButtonClick(door, "open", 0, buttonPrimaryClassNameCss,
            (wrapperInstance) => {
                wrapperInstance.state.doorOpen = true;
            });
    });

    it("Does not invoke close() when a certain button is clicked and the state of doorOpen is false", () => {
        spyOnFunctionAfterButtonClick(door, "close", 0, buttonPrimaryClassNameCss);
    });

    it("Invokes setState() once when reset() is invoked", () => {
        spyOnSetState(door, 1, "reset");
    });

    it("Invokes setState() once when open() is invoked", () => {
        spyOnSetState(door, 1, "open");
    });

    it("Invokes setState() once when close() is invoked", () => {
        spyOnSetState(door, 1, "close");
    });

    it("Changes text to the string representation of the score when open() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateText = wrapperInstance.state.text;

        wrapperInstance.open();

        const updatedStateScore = wrapperInstance.state.score;

        expect(wrapperInstance.state.text).not.toEqual(defaultStateText);
        expect(wrapperInstance.state.text).toEqual(updatedStateScore.toString());
    });

    it("Changes doorOpen to true when open() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateDoorOpen = wrapperInstance.state.doorOpen;

        wrapperInstance.open();

        expect(wrapperInstance.state.doorOpen).not.toEqual(defaultStateDoorOpen);
        expect(wrapperInstance.state.doorOpen).toEqual(true);
    });

    it("Changes doorClass to a certain class when open() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateDoorClass = wrapperInstance.state.doorClass;

        wrapperInstance.open();

        expect(wrapperInstance.state.doorClass).not.toEqual(defaultStateDoorClass);
        expect(wrapperInstance.state.doorClass).toEqual(doorOpenClassName);
    });

    it("Changes score to the sum of its previous value and a random number when open() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const spyRandom = jest.spyOn(Math, "random").mockImplementation(() => 0.5);

        wrapperInstance.state.score = 1;

        wrapperInstance.open();

        expect(wrapperInstance.state.score).toEqual(6);
        expect(spyRandom).toHaveBeenCalledTimes(1);
    });

    it("Changes score to a negative value when a random number less than 0.5 is returned after invoking open()",
        () => {
            const wrapper = shallow(door);

            const wrapperInstance = wrapper.instance();

            jest.spyOn(Math, "random").mockReset();

            const spyRandom = jest.spyOn(Math, "random").mockImplementation(() => 0.4);

            wrapperInstance.state.score = 1;

            wrapperInstance.open();

            expect(wrapperInstance.state.score).toEqual(-3);
            expect(spyRandom).toHaveBeenCalledTimes(1);
        });

    it("Changes text to its default value when close() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateText = wrapperInstance.state.text;

        wrapperInstance.state.text = "1";

        expect(wrapperInstance.state.text).not.toEqual(defaultStateText);

        wrapperInstance.close();

        expect(wrapperInstance.state.text).toEqual(defaultStateText);
    });

    it("Changes doorClass to its default value when close() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateDoorClass = wrapperInstance.state.doorClass;

        wrapperInstance.state.doorClass = doorOpenClassName;

        expect(wrapperInstance.state.doorClass).not.toEqual(defaultStateDoorClass);

        wrapperInstance.close();

        expect(wrapperInstance.state.doorClass).toEqual(defaultStateDoorClass);
    });

    it("Changes doorOpen to its default value when close() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateDoorOpen = wrapperInstance.state.doorOpen;

        wrapperInstance.state.doorOpen = true;

        expect(wrapperInstance.state.doorOpen).not.toEqual(defaultStateDoorOpen);

        wrapperInstance.close();

        expect(wrapperInstance.state.doorOpen).toEqual(defaultStateDoorOpen);
    });

    it("Does not change score to its default value when close() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateScore = wrapperInstance.state.score;

        wrapperInstance.state.score = 1;

        expect(wrapperInstance.state.score).not.toEqual(defaultStateScore);

        wrapperInstance.close();

        expect(wrapperInstance.state.score).not.toEqual(defaultStateScore);
    });

    it("Changes text to its default value when reset() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateText = wrapperInstance.state.text;

        wrapperInstance.state.text = "1";

        expect(wrapperInstance.state.text).not.toEqual(defaultStateText);

        wrapperInstance.reset();

        expect(wrapperInstance.state.text).toEqual(defaultStateText);
    });

    it("Changes doorClass to its default value when reset() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateDoorClass = wrapperInstance.state.doorClass;

        wrapperInstance.state.doorClass = doorOpenClassName;

        expect(wrapperInstance.state.doorClass).not.toEqual(defaultStateDoorClass);

        wrapperInstance.reset();

        expect(wrapperInstance.state.doorClass).toEqual(defaultStateDoorClass);
    });

    it("Changes doorOpen to its default value when reset() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateDoorOpen = wrapperInstance.state.doorOpen;

        wrapperInstance.state.doorOpen = true;

        expect(wrapperInstance.state.doorOpen).not.toEqual(defaultStateDoorOpen);

        wrapperInstance.reset();

        expect(wrapperInstance.state.doorOpen).toEqual(defaultStateDoorOpen);
    });

    it("Changes score to its default value when reset() is invoked", () => {
        const wrapper = shallow(door);

        const wrapperInstance = wrapper.instance();

        const defaultStateScore = wrapperInstance.state.score;

        wrapperInstance.state.score = 1;

        expect(wrapperInstance.state.score).not.toEqual(defaultStateScore);

        wrapperInstance.reset();

        expect(wrapperInstance.state.score).toEqual(defaultStateScore);
    });
});