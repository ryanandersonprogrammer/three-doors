/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 Hallway Tests
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import {
    assertMatch,
    assertExistenceOfComponent,
    mount,
    React,
    renderer,
    shallow,
    spyOnFunctionAfterButtonClick
} from "../../three-doors-test-setup";
import Hallway from "../../../src/components/Hallway";
import Door from "../../../src/components/Door";

describe("Hallway", () => {
    const numberOfDoors = 3;
    const doorOpenClassName = "door-open-test";
    const doorClosedClassName = "door-closed-test";
    const buttonPrimaryClassName = "btn-test btn-primary-test";
    const buttonSecondaryClassName = "btn-test btn-secondary-test";
    const buttonSecondaryClassNameCss = ".btn-test.btn-secondary-test";
    const hallway = <Hallway numberOfDoors={numberOfDoors} doorOpenClassName={doorOpenClassName}
                             doorClosedClassName={doorClosedClassName} buttonPrimaryClassName={buttonPrimaryClassName}
                             buttonSecondaryClassName={buttonSecondaryClassName}/>;
    const hallwayWithoutClasses = <Hallway numberOfDoors={numberOfDoors}/>;

    const assertDoorCount = (hallway, numberOfDoors) => {
        const wrapper = shallow(hallway);

        const doors = wrapper.find(Door);

        assertExistenceOfComponent(doors, numberOfDoors);
    };

    it("Renders correctly when numberOfDoors is supplied", () => {
        const component = renderer.create(
            <Hallway numberOfDoors={numberOfDoors}/>
        );

        assertMatch(component);
    });

    it("Renders correctly when numberOfDoors and doorOpenClassName are supplied", () => {
        const component = renderer.create(
            <Hallway numberOfDoors={numberOfDoors} doorOpenClassName={doorOpenClassName}/>
        );

        assertMatch(component);
    });

    it("Renders correctly when numberOfDoors, doorOpenClassName, and doorClosedClassName are supplied",
        () => {
            const component = renderer.create(
                <Hallway numberOfDoors={numberOfDoors} doorOpenClassName={doorOpenClassName}
                         doorClosedClassName={doorClosedClassName}/>
            );

            assertMatch(component);
        });

    it("Renders correctly when numberOfDoors, doorOpenClassName, doorClosedClassName, and " +
        "buttonPrimaryClassName are supplied", () => {
        const component = renderer.create(
            <Hallway numberOfDoors={numberOfDoors} doorOpenClassName={doorOpenClassName}
                     doorClosedClassName={doorClosedClassName}
                     buttonPrimaryClassName={buttonPrimaryClassName}/>
        );

        assertMatch(component);
    });

    it("Renders correctly when numberOfDoors, doorOpenClassName, doorClosedClassName, " +
        "buttonPrimaryClassName, and buttonSecondaryClassName are supplied", () => {
        const component = renderer.create(hallway);

        assertMatch(component);
    });

    it("Adds .btn-test and .btn-secondary-test to a button during rendering", () => {
        let button;

        const wrapperWithoutSecondaryClass = shallow(hallwayWithoutClasses);

        const wrapperWithSecondaryClass = shallow(
            <Hallway numberOfDoors={numberOfDoors} buttonSecondaryClassName={buttonSecondaryClassName}/>
        );

        button = wrapperWithoutSecondaryClass.find(buttonSecondaryClassNameCss);

        assertExistenceOfComponent(button, 0);

        button = wrapperWithSecondaryClass.find(buttonSecondaryClassNameCss);

        assertExistenceOfComponent(button, 1);
    });

    it("Invokes close() once when a certain button is clicked", () => {
        spyOnFunctionAfterButtonClick(hallway, "close", 1,
            buttonSecondaryClassNameCss);
    });

    it("Renders three instances of Door when numberOfDoors is 3", () => {
        assertDoorCount(hallwayWithoutClasses, numberOfDoors);
    });

    it("Renders three instances of Door when numberOfDoors is 3.4", () => {
        assertDoorCount(<Hallway numberOfDoors={3.4}/>, numberOfDoors);
    });

    it("Renders three instances of Door when numberOfDoors is -3.4", () => {
        assertDoorCount(<Hallway numberOfDoors={-3.4}/>, numberOfDoors);
    });

    it("Renders three instances of Door when numberOfDoors is 3.8", () => {
        assertDoorCount(<Hallway numberOfDoors={3.8}/>, numberOfDoors);
    });

    it("Renders three instances of Door when numberOfDoors is -3.8", () => {
        assertDoorCount(<Hallway numberOfDoors={-3.8}/>, numberOfDoors);
    });

    it("Updates its state with the sum of the scores from each instance of Door and invokes reset() on each " +
        "instance of Door when a certain button is clicked to invoke close()", () => {
        const wrapper = mount(hallway);

        const wrapperInstance = wrapper.instance();

        const doors = wrapper.find(Door);

        assertExistenceOfComponent(doors, 3);

        const doorFirst = doors.at(0);
        const doorSecond = doors.at(1);
        const doorThird = doors.at(2);

        const doorFirstInstance = doorFirst.instance();
        const doorSecondInstance = doorSecond.instance();
        const doorThirdInstance = doorThird.instance();

        doorFirstInstance.state.score = 1;
        doorSecondInstance.state.score = 2;
        doorThirdInstance.state.score = 3;

        const spyDoorResetFirst = jest.spyOn(doorFirstInstance, "reset");
        const spyDoorResetSecond = jest.spyOn(doorSecondInstance, "reset");
        const spyDoorResetThird = jest.spyOn(doorThirdInstance, "reset");

        wrapperInstance.doorRefs[0].current = doorFirstInstance;
        wrapperInstance.doorRefs[1].current = doorSecondInstance;
        wrapperInstance.doorRefs[2].current = doorThirdInstance;

        const button = wrapper.find(buttonSecondaryClassNameCss);

        assertExistenceOfComponent(button, 1);

        button.simulate("click");

        expect(wrapperInstance.state.score).toEqual(6);
        expect(spyDoorResetFirst).toHaveBeenCalledTimes(1);
        expect(spyDoorResetSecond).toHaveBeenCalledTimes(1);
        expect(spyDoorResetThird).toHaveBeenCalledTimes(1);
    });
});