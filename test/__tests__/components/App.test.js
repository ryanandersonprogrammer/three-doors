/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 App Tests
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import {assertMatch, assertExistenceOfComponent, React, renderer, shallow} from "../../three-doors-test-setup";
import App from "../../../src/components/App";
import Hallway from "../../../src/components/Hallway";

describe("App", () => {
    const app = <App/>;

    it("Renders correctly", () => {
        const component = renderer.create(app);

        assertMatch(component);
    });

    it("Renders one instance of Hallway", () => {
        const wrapper = shallow(app);

        const hallway = wrapper.find(Hallway);

        assertExistenceOfComponent(hallway, 1);
    });
});