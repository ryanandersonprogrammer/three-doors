/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 Test Setup
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import React from "react";
import renderer from "react-test-renderer";
import {configure, mount, shallow} from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({adapter: new Adapter()});

const assertMatch = (component) => {
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
};

const assertExistenceOfComponent = (component, expectedLength) => {
    expect(component).toBeDefined();
    expect(component).toHaveLength(expectedLength);
};

const spyOnFunctionAfterButtonClick = (component, functionName, expectedNumberOfCalls, classSelector, setState) => {
    const wrapper = shallow(component);

    const wrapperInstance = wrapper.instance();

    const spyFunction = jest.spyOn(wrapperInstance, functionName).mockImplementation(jest.fn);

    if (typeof setState === "function")
        setState(wrapperInstance);

    wrapperInstance.forceUpdate();

    const button = wrapper.find(classSelector);

    assertExistenceOfComponent(button, 1);

    button.simulate("click");

    expect(spyFunction).toHaveBeenCalledTimes(expectedNumberOfCalls);
};

const spyOnSetState = (component, expectedNumberOfCalls, stateAlteringFunctionName) => {
    if (typeof stateAlteringFunctionName !== "string")
        throw "The name of the function that alters state must be a string.";

    const wrapper = shallow(component);

    const wrapperInstance = wrapper.instance();

    const spySetState = jest.spyOn(wrapperInstance, "setState");

    const stateAlteringFunction = wrapperInstance[stateAlteringFunctionName];

    stateAlteringFunction();

    expect(spySetState).toHaveBeenCalledTimes(expectedNumberOfCalls);
};

export {
    assertExistenceOfComponent,
    assertMatch,
    mount,
    React,
    renderer,
    shallow,
    spyOnFunctionAfterButtonClick,
    spyOnSetState
};