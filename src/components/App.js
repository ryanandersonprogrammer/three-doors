/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 App Component
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import React, {Component} from "react";
import "./../css/App.css";
import Hallway from "./Hallway";

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="wrapper">
                <div className="container bg-white">
                    <h1>Three Doors</h1>
                    <p className="headline-text">Pick a door!</p>
                    <Hallway buttonPrimaryClassName="btn btn-primary" buttonSecondaryClassName="btn btn-secondary"
                             doorClosedClassName="door-closed"
                             doorOpenClassName="door-open" numberOfDoors={3}/>
                </div>
            </div>
        );
    }
}