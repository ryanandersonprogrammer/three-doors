/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 Door Component
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import React, {Component} from "react";
import PropTypes from "prop-types";
import "./../css/Door.css";

class Door extends Component {
    constructor(props) {
        super(props);

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.reset = this.reset.bind(this);

        this.state = {
            text: this.props.doorText,
            doorClass: this.props.doorClosedClassName,
            doorOpen: false,
            score: 0
        };
    }

    open() {
        let state;
        let uniformRandomNumber;

        uniformRandomNumber = Math.random();

        if (uniformRandomNumber < 0.5)
            uniformRandomNumber = -uniformRandomNumber;

        const numberBetweenZeroAndNine = uniformRandomNumber * 10;

        const randomNumber = numberBetweenZeroAndNine < 0 ? Math.ceil(numberBetweenZeroAndNine) :
            Math.floor(numberBetweenZeroAndNine);

        state = this.state.score + randomNumber;

        this.setState({
            text: randomNumber.toString(),
            doorClass: this.props.doorOpenClassName,
            doorOpen: true,
            score: state
        });
    }

    close() {
        this.setState({text: this.props.doorText, doorClass: this.props.doorClosedClassName, doorOpen: false});
    }

    reset() {
        this.setState({
            text: this.props.doorText,
            doorClass: this.props.doorClosedClassName,
            doorOpen: false,
            score: 0
        });
    }

    render() {
        return (
            <div className="door-group">
                <div className={`door ${this.state.doorClass}`}>
                    <h1>Door #{this.props.doorNumber}</h1>
                    <p>{this.state.text}</p>
                </div>
                <button className={this.props.buttonPrimaryClassName}
                        onClick={this.state.doorOpen ? this.close : this.open}>
                    {this.state.doorOpen ? "Close" : "Open"}
                </button>
            </div>
        );
    }
}

Door.propTypes = {
    doorNumber: PropTypes.number.isRequired,
    doorText: PropTypes.string,
    doorClosedClassName: PropTypes.string,
    doorOpenClassName: PropTypes.string,
    buttonPrimaryClassName: PropTypes.string
};

export default Door;