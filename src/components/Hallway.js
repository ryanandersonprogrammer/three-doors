/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 Hallway Component
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import React, {Component} from "react";
import PropTypes from "prop-types";
import "./../css/Hallway.css";
import Door from "./Door";

class Hallway extends Component {
    constructor(props) {
        super(props);

        this.state = {score: 0};
        this.doors = [];
        this.doorRefs = [React.createRef(), React.createRef(), React.createRef()];

        this.close = this.close.bind(this);
    }

    close() {
        let score;

        score = 0;

        this.doorRefs.forEach(d => {
            const ref = d.current;

            score += ref.state.score;

            ref.reset();
        });

        this.setState({score: score});
    }

    render() {
        if (this.doors.length === 0) {
            const numberOfDoors = Math.floor(Math.abs(this.props.numberOfDoors)) + 1;

            for (let i = 1; i < numberOfDoors; i++)
                this.doors.push(<Door ref={this.doorRefs[i - 1]} key={`door-${i}`}
                                      buttonPrimaryClassName={this.props.buttonPrimaryClassName}
                                      doorClosedClassName={this.props.doorClosedClassName}
                                      doorOpenClassName={this.props.doorOpenClassName} doorNumber={i}
                                      doorText={`What's behind Door #${i}?`}/>);
        }

        return (
            <div>
                <div className="door-container">
                    {this.doors}
                </div>
                <button className={this.props.buttonSecondaryClassName} onClick={this.close}>Close</button>
                <p>{this.state.score}</p>
            </div>
        );
    }
}

Hallway.propTypes = {
    numberOfDoors: PropTypes.number.isRequired,
    buttonPrimaryClassName: PropTypes.string,
    buttonSecondaryClassName: PropTypes.string,
    doorClosedClassName: PropTypes.string,
    doorOpenClassName: PropTypes.string
};

export default Hallway;