/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 Create Index from Template Plug-In
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import ejs from "ejs";

class ThreeDoorsCreateIndexFromTemplatePlugIn {
    constructor(options) {
        this.options = {
            bundlePath: options.bundlePath,
            indexAbout: options.indexAbout,
            indexLicense: options.indexLicense,
            indexOutputFilename: options.indexOutputFilename,
            indexTemplatePath: options.indexTemplatePath,
            templateStylePaths: options.templateStylePaths
        };
    }

    apply(compiler) {
        if (compiler.hooks) {
            compiler.hooks.emit.tapAsync("ThreeDoorsCreateIndexFromTemplatePlugIn", (compilation, callback) => {
                ejs.renderFile(
                    this.options.indexTemplatePath,
                    {
                        bundlePath: this.options.bundlePath,
                        indexAbout: this.options.indexAbout,
                        indexLicense: this.options.indexLicense,
                        templateStylePaths: this.options.templateStylePaths
                    },
                    {
                        async: false,
                        rmWhitespace: false
                    },
                    (error, html) => {
                        if (!error) {
                            compilation.assets[this.options.indexOutputFilename] = {
                                source: () => html,
                                size: () => html.length
                            };
                        }
                    }
                );

                callback();
            });
        }
    }
}

export default ThreeDoorsCreateIndexFromTemplatePlugIn;