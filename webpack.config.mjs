/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Three Doors v0.1.0 Package Bundle Configuration
 *
 * by Ryan E. Anderson
 *
 * Copyright (c) 2020 Ryan E. Anderson
 */
import path from "path";
import {CleanWebpackPlugin} from "clean-webpack-plugin";
import ThreeDoorsCreateIndexFromTemplatePlugIn from "./plug-in/three-doors-create-index-from-template";
import ESLintWebpackPlugin from "eslint-webpack-plugin";

const options = process.argv;
const mode = options[options.indexOf("--mode") + 1];
const licenseText = `Copyright 2020 Ryan E. Anderson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.`;
const aboutText = `    Three Doors v0.1.0 Index ${mode.charAt(0).toUpperCase()}${mode.substr(1)}

    by Ryan E. Anderson
                
    Copyright (C) 2020 Ryan E. Anderson`;
const templateStyle = `css/style.${mode === "production" ? "min." : ""}css`;
const webpackConfiguration = {
    entry: path.join(__dirname, "src/index.js"),
    output: {
        filename: "bundle.js",
        path: path.join(__dirname, `dist${options.indexOf("--hot") === -1 ? mode === "development" || mode === "none" ? "-development" : "" : "-temporary"}`)
    },
    devtool: 'eval-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    "babel-loader"
                ]
            },
            {
                test: /\.css$/,
                exclude: /style(?:\.min)?\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /LICENSE$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name]"
                    }
                }
            },
            {
                test: /\.(?:ico|md)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]"
                    }
                }
            },
            {
                test: /\.png$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "media/png"
                    }
                }
            },
            {
                test: /\.css$/,
                exclude: /(?:App|Door|Hallway)\.css$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "css"
                    }
                }
            }
        ]
    },
    performance: {
        maxAssetSize: 5000000,
        maxEntrypointSize: 5000000
    },
    plugins: [
        new CleanWebpackPlugin(),
        new ESLintWebpackPlugin({
            eslintPath: require.resolve("eslint"),
            extensions: ["js", "mjs"]
        }),
        new ThreeDoorsCreateIndexFromTemplatePlugIn({
            bundlePath: "bundle.js",
            indexAbout: aboutText,
            indexLicense: licenseText,
            indexOutputFilename: "index.html",
            indexTemplatePath: "src/index.ejs",
            templateStylePaths: [
                templateStyle
            ]
        })
    ]
};

export default webpackConfiguration;